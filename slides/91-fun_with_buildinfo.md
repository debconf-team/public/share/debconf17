Fun with .buildinfo
===================

  * Steven Chamberlain <<stevenc@debian.org>>
  * DebConf17, Montreal

---

Motivations
-----------

  * How reproducible are Debian's packages?

    * 94% of the packages in stretch, are _potentially_ reproducible
      (if we ignore the build path issue)

    * Let's try to _actually_ reproduce the official packages!

---

What is a .buildinfo file
-------------------------

  * Description of the build environment

  * Debug information, which might help to explain non-reproducibility

  * Generated at the end of a Debian package build;
    on the developer's machines, and on Debian's _buildds_ (build servers)

    * DDs can find these on _coccia.debian.org:_
      _/srv/ftp-master.debian.org/buildinfo_

---

Causes of non-reproducibility (I)
---------------------------------

  * Built binary packages might vary due to:
    * specifics of the machine/person building the package
      (username, hostname, CPU model)
    * time (recorded as timestamps in the output)
    * randomness (readdir order, memory allocations)

---

Causes of non-reproducibility (II)
----------------------------------

  * Hard to fix:
    * Build path

  * Intentional or expected:
    * DEB_BUILD_OPTIONS, DEB_BUILD_PROFILES
    * Versions of installed build dependencies

---

A formula for reproducing the build
-----------------------------------

source + _buildinfo_ &rarr; reproducible binaries

---

Specification
-------------

See deb-buildinfo(5):

    Format: 1.0
    Source:     (source package name)
    Version:    (source package version)
    Binary:     (binary package names)
    Checksums-*: (SHA-256 hashes)
    Build-Path: /build/wherever
    Installed-Build-Depends:
     autoconf (= 2.69-10),
     gcc-6 (= 6.3.0-2),
     make (= 4.1-9),
     ...
    Environment:
     DEB_BUILD_OPTIONS="parallel=4"
     LANG="en_US.UTF-8"
     ...

  * Optionally GPG-signed (by the person/machine building)

---

Reproducing Debian's official packages
--------------------------------------

  * Can specify a build path to _sbuild_, with the _--build-path_ option

  * Need to install (or downgrade to) specific versions of build-deps:
    I've used _snapshot.debian.org_

---

Live demo
---------

---

Early findings
--------------

  * 9 out of 10 builds could be reproduced!

  * We still use binaries from 2011, when building new packages!
    * sometimes built on developer's own machines
    * please consider source-only uploads now

  * We see a .buildinfo for developers' builds:
    * gcc-4.9 still installed in schroot (coreutils)
    * one package could not be reproduced (libidn2-0-dev)

  * 2 .buildinfo files were incomplete (bug, probably fixed now)

---

Next steps
----------

  * Set up dedicated rebuilders building Debian sid packages

  * Add buildinfo of those builds to a central repository:
    _buildinfo.debian.net_ for example
    * user-facing tools may fetch signatures from here

---

Future benefits
---------------

  * Securing the software supply chain:

source &rarr; buildd &rarr; .deb archive &rarr; mirror &rarr; user

source &rarr; developer &rarr; signed .buildinfo &rarr; user

source &rarr; rebuilders &rarr; signed .buildinfo &rarr; user

  * In-depth defence against compromise of:
    * APT fetcher
    * archive signing key
    * developer's build machine
    * official buildds

---

More ideas (I)
--------------

  * Re-use the buildinfo format as the definition of a build "job":
    specify a package and architecture, and some machine will build it

  * tests.r-b.org no longer needs to schedule build1+build2 together as
    a pair?

---

More ideas (II)
---------------

  * Test effect of environment variables (does "nocheck" vary the
    output binaries?)

  * Build with specific packages removed from the build enviroment
    (detect unnecessary Build-Depends)

  * Check whether cross-compiled packages are different from
    natively-built

---

Questions?
----------

  * Mailing list:

    reproducible-builds@lists.alioth.debian.org

  * IRC:  #debian-reproducible on OFTC

